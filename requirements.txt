# Core requirements
numpy
scipy>1.0.0
matplotlib>=1.1.0
pyyaml>=5.1
pandas
joblib
# Non-essential developer requirements
setuptools>=41.0.1
flake8
sphinx_gallery
numpydoc
pydata-sphinx-theme
myst-parser
ipywidgets
coverage
pytest
pytest-cov
